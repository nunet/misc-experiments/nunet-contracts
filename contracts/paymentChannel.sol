// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/access/Ownable.sol";


contract UniDirectionalPaymentChannel is ReentrancyGuard, Ownable {
    using ECDSA for bytes32;

    struct PaymentMetaData {
        uint256 payment;
        bool postPaid;
        uint256 vaildTill;
        address sender;
    }

    mapping(address=>PaymentMetaData) public balanceBook;

    event FundsDeposited(uint256 payment, bool postPaid,uint256 vaildTill, address sender);
    event FundsWithdrawn(uint256 payment, bool postPaid,uint256 vaildTill, address reciver);


    function deposit(PaymentMetaData memory metaData) public payable{
        require(metaData.payment == msg.value,"The value needs to be equal to amount");
        require(metaData.vaildTill <= block.timestamp,"The expiration need to be greater than current time");
        require(metaData.sender == msg.sender, "Invalid request: Message sender needs to be same in meta data");
        balanceBook[msg.sender] = metaData;
        metaData.sender = msg.sender;
        emit FundsDeposited(metaData.payment,metaData.postPaid,metaData.vaildTill,msg.sender);
    }

    function getHash(PaymentMetaData memory metaData) public view returns (bytes32){
        return _getHash(metaData);
    }

    function _getHash(PaymentMetaData memory metaData) private view returns (bytes32) {
        return keccak256(abi.encode(address(this), metaData));
    }

    function _getEthSignedHash(PaymentMetaData memory metaData) private view returns (bytes32) {
        return _getHash(metaData).toEthSignedMessageHash();
    }

    function _verify(PaymentMetaData memory metaData, bytes memory _sig) private view returns (bool) {
        return _getEthSignedHash(metaData).recover(_sig) == metaData.sender;
    }

    function verify(PaymentMetaData memory metaData, bytes memory _sig) external view returns (bool) {
        return _verify(metaData, _sig);
    }

    function withdraw(PaymentMetaData memory metaData, bytes memory _sig) external nonReentrant {
        require(_verify(metaData, _sig), "invalid signature");
        require(metaData.vaildTill < block.timestamp,"Token Expired");
        require(balanceBook[metaData.sender].payment == metaData.payment,"Invalid request:payment in balance book needs to be same as meta data");
        require(metaData.sender != msg.sender,"Invalid request:You can not withdraw your own funds");
        require(balanceBook[metaData.sender].sender != msg.sender,"Invalid request:You can not withdraw your own funds");
        (bool sent, ) = (msg.sender).call{value: metaData.payment}("");
        require(sent, "Failed to send Ether");
        delete balanceBook[metaData.sender];
    }

}
