const { expect } = require("chai");
const {ethers} =  require("hardhat");


describe("Payment Channel", function () {
    
  it("Deploy payment channel", async function () {
    const [owner] = await ethers.getSigners();

    const paymentChannel = await ethers.getContractFactory("UniDirectionalPaymentChannel");
    console.log(paymentChannel)
     expect(await paymentChannel.owner()).to.equal(owner.address);
  });
});